from typing import Sequence

def minus_minus(string: str) -> str:
    if string[0] == "-":
        string = string[1:]
    return string


def if_possible_int(string: str) -> bool:
    """Принимает строку string, анализирует возможность преобразования ее в целое число.

    Если это возможно - возврвщает True
    если это невозможно - возвращает False
    """
    string = minus_minus(string)
    return string.isdigit()


def if_possible_float(string: str, separators: str = ".,:") -> str | None:
    """Принимает строку string, анализирует возможностьпреобразования ее в число типа float.
    """
    leader_minus = "-" if string[0] == "-" else ""
    string = minus_minus(string)
    string = string.translate(str.maketrans(separators, "." * len(separators)))
    parts = string.partition(".")
    if parts[0].isdigit() and parts[2].isdigit():
        return leader_minus + string


def input_data(message: str) -> int | float:
    while True:
        a = input(message)
        prepared_a = if_possible_float(a)
        if if_possible_int(a):
            return int(a)
        elif prepared_a:
            return float(prepared_a)
        print(f"введенное Вами значение - '{a}' - не может быть преобразовано в число.\nДля ввода значения используйте только цифры, точку в качестве десятичного разделителя и минус, если число отрицательное")


if __name__ == "__main__":
    message = "Введите число: "
    a = input_data(message)
    print(a)
